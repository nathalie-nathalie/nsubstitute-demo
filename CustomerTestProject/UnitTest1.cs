using NSubstitute;
using NUnit.Framework;
using CustomerProject;
using CalculatorProject;
using System;
using ControlerProject;

namespace CustomerTestProject
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            //**************ARRANGE
            var dependencies = Substitute.For<ICustomerServiceDependencies>();
            var guid = Guid.NewGuid();
            var now = DateTime.UtcNow;
            var name = "customer1";

            //define behavior of substitute
            dependencies.NewId().Returns(guid);
            dependencies.GetUtcNow().Returns(now);

            var testee = new CustomerService(dependencies);

            //**************ACT
            var customer = testee.RegisterCustomer(name);

            //*************ASSERT
            Assert.AreEqual(guid, customer.Id);
            Assert.AreEqual(now, customer.CreatedAt);
            Assert.AreEqual(name, customer.Name);

        }

        [Test]
        public void TestCalculatorSettingReturnValue()
        {
            //create substitute
            var calculatorSub = Substitute.For<ICalculator>();

            //define behavior of substitute
            calculatorSub.Add(1, 2).Returns(3);

            Assert.AreEqual(3, calculatorSub.Add(1, 2));
            Assert.AreNotEqual(3, calculatorSub.Add(1, 1));
        }

        [Test]
        public void TestCalculatorAnyArgType()
        {
            //create substitute
            var calculatorSub = Substitute.For<ICalculator>();

            //define behavior of substitute
            calculatorSub.Multiply(Arg.Any<double>(), 0.0001).Returns(0.0);

            Assert.AreEqual(0.0, calculatorSub.Multiply(5.7, 0.0001));
        }

        [Test]
        public void TestCalculatorSpecifyArg()
        {
            //create substitute
            var calculatorSub = Substitute.For<ICalculator>();

            //define behavior of substitute
            calculatorSub.Validate(Arg.Is<int>(x => x<0)).Returns("negative");


            Assert.AreEqual("negative", calculatorSub.Validate(-5));
        }

        [Test]
        public void TestCalculatorFunctionReturn() {
            //create substitute
            var calculatorSub = Substitute.For<ICalculator>();

            //define behavior of substitute
            calculatorSub.Add(Arg.Any<int>(), Arg.Any<int>()).Returns(x => (int)x[0] + (int)x[1]);

            Assert.AreEqual(10, calculatorSub.Add(2, 8));
        }

        [Test]
        public void TestController() {
            //**************ARRANGE
            //create substitute
            var connection = Substitute.For<IConnection>();
            var command = Substitute.For<ICommand>();
            var testee = new Controller(connection, command);
            command.When(x => x.Run()).Do(x => throw new Exception());

            //**************ACT
            Assert.Throws<Exception>(() => testee.Do());

            //*************ASSERT
            Received.InOrder(() => {
                connection.Open();
                command.Run();
                connection.Close();
            });

        }
    }
}