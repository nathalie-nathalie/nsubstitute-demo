﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;


[assembly: InternalsVisibleTo("CustomerTestProject")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]

namespace CalculatorProject
{
    interface ICalculator
    {
        int Add(int a, int b);

        double Multiply(double a, double b);

        string Validate(int a);
    }
}
