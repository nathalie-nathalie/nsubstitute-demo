﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

[assembly: InternalsVisibleTo("CustomerTestProject")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]

namespace ControlerProject
{
    class Controller
    {
        private readonly IConnection _connection;
        private readonly ICommand _command;

        public Controller(IConnection connection, ICommand command) {
            _connection = connection;
            _command = command;
        }

        public void Do() {
            _connection.Open();
            try
            {
                _command.Run();
            }
            finally
            {
                _connection.Close();
            }
        }
    }
}
