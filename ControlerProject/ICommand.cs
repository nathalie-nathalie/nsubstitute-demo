﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ControlerProject
{
    interface ICommand
    {
        void Run();
    }
}
