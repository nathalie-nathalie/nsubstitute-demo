﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ControlerProject
{
    interface IConnection
    {
        void Open();
        void Close();
    }
}
