﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerProject
{
    class CustomerServiceDependencies : ICustomerServiceDependencies
    {
        public DateTime GetUtcNow() => DateTime.UtcNow;

        public Guid NewId() => Guid.NewGuid();
    }
}
