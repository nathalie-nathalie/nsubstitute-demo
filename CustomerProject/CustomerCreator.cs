﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerProject
{
    class CustomerCreator
    {
        static void Main(string[] args)
        {
            var customer = new CustomerService(new CustomerServiceDependencies()).RegisterCustomer("real customer");
            
            Console.WriteLine("new customer: "+customer.Name+"; created at: "+ customer.CreatedAt + "; customerId: " + customer.Id);
            Console.Read();
        }
    }
}
