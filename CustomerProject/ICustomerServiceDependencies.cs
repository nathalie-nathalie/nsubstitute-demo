﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

[assembly: InternalsVisibleTo("CustomerTestProject")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]

namespace CustomerProject
{
    
    interface ICustomerServiceDependencies
    {
        Guid NewId();
        DateTime GetUtcNow();
    }
}
