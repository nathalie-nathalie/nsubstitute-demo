﻿using System;

namespace CustomerProject
{
    class CustomerService: ICustomerService
    {
        private readonly ICustomerServiceDependencies _dependencies;
        public CustomerService(ICustomerServiceDependencies dependencies) {
            _dependencies = dependencies;
        }
        public Customer RegisterCustomer(string name)
        {
            return new Customer(_dependencies.NewId(), _dependencies.GetUtcNow(), name);
        }

    }
}
