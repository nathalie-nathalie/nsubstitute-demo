﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerProject
{
    class Customer
    {
        public Customer(Guid id, DateTime createdAt, string name)
        {
            Id = id;
            CreatedAt = createdAt;
            Name = name;
        }

        public Guid Id { get; }
        public DateTime CreatedAt { get; }
        public string Name { get; }
    }
}
