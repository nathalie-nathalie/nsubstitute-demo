﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerProject
{
    interface ICustomerService
    {
        Customer RegisterCustomer(string name);
    }
}
